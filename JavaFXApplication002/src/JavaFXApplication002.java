/**
 * Created by sts on 22.01.2017.
 */

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

public class JavaFXApplication002 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Group root = new Group();
        Scene scene = new Scene(root, 600, 600, Color.FLORALWHITE);

        // Первая линия
        Line line1 = new Line();
        line1.setStartX(200);
        line1.setStartY(20);
        line1.setEndX(300);
        line1.setEndY(20);
        line1.setStroke(Color.BLACK);
        root.getChildren().add(line1);




        primaryStage.setTitle("JavaFX 8 Tutorial - Drawing Line and QuadCurve");
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
