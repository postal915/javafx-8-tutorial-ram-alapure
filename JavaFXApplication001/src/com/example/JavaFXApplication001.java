package com.example;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class JavaFXApplication001 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Создаем кнопку
        Button btn = new Button();
        // Пишем текст на кнопке
        btn.setText("Say 'Hello World'");
        // Обрабатываем нажатие на кнопку
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World");
            }
        });

        // Создаем поверхность на которой будет размещена кнопка
        StackPane root = new StackPane();
        // Добавляем на поверхность кнопку
        root.getChildren().add(btn);
        // Создаем сцену и задаем ее размеры
        Scene scene = new Scene(root, 300, 250);
        // Задаем заголовок окна
        primaryStage.setTitle("Hello World");
        // Устанавливаем сцену
        primaryStage.setScene(scene);
        // Показываем окно программы
        primaryStage.show();

    }
}
